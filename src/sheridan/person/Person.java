/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package sheridan.person;

/**
 *
 * @author mymac
 */
public class Person {
    
    private String name;
    private double weight;
    private double height;
    
    Person(String name, double weight, double height){
    this.name = name;
    this.height=height;
    this.weight = weight;
    }
    
    public double getHeight(){
        return height;
    }
    
    public double getWeight(){
        return weight;
    }
    
    public double getWeightLbs(){
        return weight * 2.2;
    }
    
    
    
    public double calculateBMI(){
        return weight/ ((height*height/10000));
    
    }

    
}
