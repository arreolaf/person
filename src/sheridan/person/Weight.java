/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package sheridan.person;

/**
 *
 * @author mymac
 */
public enum Weight { 
    KG(1),LB(2.2);  
    
    private int kilo;
    private double pound;
    
    private Weight(int kilo){
        this.kilo = kilo;
        
    }
    public int getKilo(){
        return kilo;
    }
    
    private Weight(double pound){
        this.pound = pound;
    }
    
     public double getPound(){
       return pound;
    }
}
