/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package sheridan.person;

/**
 *
 * @author mymac
 */
public enum Height {
    CM(100), IN(39.4);
    
    private double heightInCm;
    private double heightInIn;
    
    private Height (double heightInCm){
    this.heightInCm = heightInCm;
    }
    
    public double getHeightEnum(){
        return heightInCm;
    }
    
//    private Height (double heightInIn){
//    this.heightInIn = heightInIn;
//    }
//    
//    public double getHeightInIn(){
//        return heightInIn;
//    }
    
    
}
